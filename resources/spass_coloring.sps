begin_problem(Sokrates1).

list_of_descriptions.
name({*Sokrates*}).
author({*Christoph Weidenbach*}).
status(unsatisfiable).
description({* Sokrates is mortal and since all humans are mortal, he is mortal too. *}).
end_of_list.

list_of_symbols.
  functions[color,neighbcolor,swap,red,blue].
  predicates[Edge].
  sorts[clr,vtx].
end_of_list.

list_of_declarations.
	forall([vtx(x)],clr(color(x))).
	forall([vtx(x)],clr(neighbcolor(x))).
	forall([clr(x)],clr(swap(x))).
	forall([vtx(x)],vtx(swap(x))).
	clr(red).
	clr(blue).
end_of_list.

list_of_formulae(axioms).
	formula(forall([vtx(x),vtx(y)],implies(Edge(x,y),not(equal(color(x),color(y)))))).
	formula(forall([vtx(x)],equal(swap(color(x)),neighbcolor(x)))).
	formula(equal(swap(red),blue)).
	formula(equal(swap(blue),red)).
	formula(forall([x],or(equal(x,red),equal(x,blue),equal(x,swap(x))))).
end_of_list.

list_of_formulae(conjectures).
	formula(forall([vtx(x),vtx(y)],implies(Edge(x,y),not(equal(neighbcolor(x),neighbcolor(y)))))).
end_of_list.

end_problem.

Jo: instead of red and blue, we could also have stated that swap is a permutation of the colors:
	formula(forall([clr(x)],exists([clr(y)],equal(x,swap(y))))).
	formula(forall([clr(x)],exists([clr(y)],equal(y,swap(x))))).
	formula(forall([vtx(x)],equal(x,swap(x)))).
For some reason, this is satisfiable... Should find out why...